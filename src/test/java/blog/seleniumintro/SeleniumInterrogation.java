package blog.seleniumintro;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static blog.seleniumintro.DriversUtils.setWebDriverProperty;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SeleniumInterrogation {
    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();
    }

//    @Test
//    public void pageSourceTest() throws InterruptedException {
//        driver.get("https://practica.wantsome.ro/blog");
//        System.out.println("Current url is: ") + driver.getCurrentUrl();
//        System.out.println("Current title is: ") + driver.getTitle();
//    }

    @Test
    public void testTitle() {
        driver.get("https://practica.wantsome.ro/blog");
        String title = driver.getTitle();
        String[] words = title.split(" ");

        int numberOfWordsWIthMoreThan5Letters = 0;
        for (String word : words) {
            if (word.length() > 5) {
                numberOfWordsWIthMoreThan5Letters++;
            }
        }
        assertEquals(5, numberOfWordsWIthMoreThan5Letters);
    }

    @AfterClass
    public static void teardown() {
        driver.close();
    }
}
