package blog.seleniumintro;

import org.apache.commons.lang3.SystemUtils;

public class DriversUtils {
    public static void setWebDriverProperty(String browser) {

        switch (browser) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", getDriversPath() + "chromedriver" + getFileExtension());
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", getDriversPath() + "geckodriver" + getFileExtension());
                break;
        }
    }

    private static String getFileExtension(){
        if (SystemUtils.IS_OS_WINDOWS){
            return ".exe";
        }
        return "";
    }
    private static String getProjectPath() {
        return System.getProperty("user.dir");
    }
//getProjectPath : daca mutam proiectul
//    C:\Code\anca-selenium

    private static String getDriversPath(){
        String driversPath;

        if (SystemUtils.IS_OS_WINDOWS){
//            return getProjectPath() + "\\src\\test\\resources\\drivers\\windows\\";
            driversPath = "\\src\\test\\resources\\drivers\\windows\\";
        }
        else {
            driversPath = "/src/test/resources/drivers/mac";
        }
        return getProjectPath() + driversPath;
    }
}
