package blog.seleniumintro;

import org.apache.commons.lang3.SystemUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static blog.seleniumintro.DriversUtils.setWebDriverProperty;
import static java.lang.System.getProperty;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class MyFirstTest {

    private static WebDriver driver;
//@BeforeClass
    public static void setup() {
        setWebDriverProperty("chrome");
        driver = new ChromeDriver();
    }

    @Test
    public void openChromeBrowser() throws InterruptedException {
//        System.setProperty("webdriver.chrome.driver", "C:\\Code\\anca-selenium\\src\\test\\resources\\drivers\\windows\\chromedriver.exe");
//        inlocuita de metoda de mai jos

        setup();
//        setWebDriverProperty("chrome");
//        WebDriver driver = new ChromeDriver();

        driver.get("https://practica.wantsome.ro/blog");
//        String pageTitle = driver.getTitle();
//        System.out.println(pageTitle);

        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());

        driver.navigate().to("https://www.emag.ro");
        assertNotEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());

        driver.navigate().back();
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());

        Thread.sleep(5000);
        teardown();
    }
//@AfterClass
    public static void teardown() {
        driver.close();
    }

//    @Test
//    public void testMethods() {
//        System.out.println(getProjectPath());
//    }

//    @Test
//    public void openFirefoxBrowser() throws InterruptedException {
////        System.setProperty("webdriver.gecko.driver", "C:\\Code\\anca-selenium\\src\\test\\resources\\drivers\\windows\\geckodriver.exe");
//
//        setWebDriverProperty("firefox");
//        WebDriver driver = new FirefoxDriver();
//        driver.get("https://practica.wantsome.ro/blog");
//        String pageTitle = driver.getTitle();
//        System.out.println(pageTitle);
//        Thread.sleep(5000);
//        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
//        driver.close();
//    }
//
//    @Test
//    public void openUnitBrowser() throws InterruptedException {
//
//        WebDriver driver = new HtmlUnitDriver();
//        driver.get("https://practica.wantsome.ro/blog");
//        String pageTitle = driver.getTitle();
//        System.out.println(pageTitle);
//        Thread.sleep(5000);
//        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
//        driver.close();
//    }

}
